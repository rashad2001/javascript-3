In JavaScript, almost everything is an object.
Objects in JavaScript, just as in many other programming languages, 
can be compared to objects in real life.A JavaScript object has properties associated with it.
Like all JavaScript variables, both the object name and property name are case sensitive.
You can define a property by assigning it a value.
For example, you can create an object named myCar and give it properties named make, model and so on.